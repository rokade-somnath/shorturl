<!DOCTYPE html>
<!--[if lt IE 7 ]> <html lang="en" class="no-js ie6"> <![endif]-->
<!--[if IE 7 ]>    <html lang="en" class="no-js ie7"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en" class="no-js ie8"> <![endif]-->
<!--[if IE 9 ]>    <html lang="en" class="no-js ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html lang="en" class="no-js"> <!--<![endif]-->
<head>
	<meta charset="utf-8">
	<!--[if IE]><![endif]-->
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<title></title>
	<meta name="description" content="">
	<meta name="keywords" content="" />
	<meta name="author" content="">
	<meta name="viewport" content="width=device-width; initial-scale=1.0">
	<!-- !CSS -->
	<link rel="stylesheet" href="/css/style.css?v=1">
</head>
<!-- !Body -->
<body>
	<div id="container">
		<h1>URL Shortener</h1>
		<section id="main">
		<?php

		    //load the form helper for creating form 
			$this->load->helper('form'); 

			echo form_open('');
			echo form_label('URL to Shorten', 'url');
			echo form_input('url');
			echo form_submit('shorty','Get Shorty');
			echo form_close();

			if(isset($short_url))
			{
				//to display short url
				echo '<a href="'.base_url().'index.php/'.$short_url.'" target="_blank" class="shorty_url">'.base_url().$short_url.'</a>';
			}

			if(isset($error))
			{
				//to display error
				echo '<div class="errors">'.$error.'</div>';
			}
			
			//to display records in tabular format
?>
</br></br></br>
  <table border="1">
	  <tr>
		<th>Sr.no</th>
		<th>Long URL</th>
		<th>Short URL</th>
		<th>Count</th>
	  </tr>
	  <?php 
	 
		if($report_data)
		{
			
			$cnt=1;
			foreach($report_data as $fdata){
				echo '<tr>';		
					
				echo'<td>'.$cnt.'</td>';
				echo'<td>'.$fdata['long_url'].'</td>';
				echo'<td><a href="'.base_url().'index.php/'.str_replace('=','-', base64_encode($fdata['id'])).'"target="_blank">'.base_url().str_replace('=','-',base64_encode($fdata['id'])).'</a></td>';
				echo'<td>'.$fdata['count'].'</td>';
				echo'</tr>';
				$cnt++;
			
			}
			
		}else{
			
			echo '<tr>';		
			echo'<td colspan="4">Records not found</td>';
			echo'</tr>';
		}
	  ?>
   </table> 
	<p><?php echo $links; ?></p>
		</section><!-- /main -->

	</div><!--!/#container -->
</body>
</html>