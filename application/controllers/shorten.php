<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Shorten extends CI_Controller {

    public function index() //called by default
    {
		$this->load->model('short_url_model');//load the model which deals with data for short URLs
		
		// load Pagination library
        $this->load->library('pagination');
         
        // load URL helper
        $this->load->helper('url');
		
		$data=array(); //data to be sent to the view�
		
        if($this->input->post('url'))//did the user post a URL to be shorten?
        {

            
            $short_url=$this->short_url_model->store_long_url();//store the URL and get back the shorten URL
            if($short_url)
            {
                $data['short_url']=$short_url;
            }
            else //there was an error
            {
                $data['error']=validation_errors();

            }
        }
		
		
		//pagination config
        $config = array();
        $config["base_url"] = base_url();
        $config["total_rows"] = $this->short_url_model->record_count();
        $config["per_page"] = 12;
        $config["uri_segment"] = 3;
		
		//pagination initialize
        $this->pagination->initialize($config);	
		$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
		
		//pass the records to view
		$report_data=$this->short_url_model->get_report_data_url($config["per_page"], $page);//get the all records for the report 
		$data["links"] = $this->pagination->create_links();
		$data['report_data']=$report_data; 
		$this->load->view('get_url', $data);//load the single view get_url and send any data to it
    }

    public function get_shorty() //this function is called by the routes file using the 404_override�
    {

        
        $this->load->model('short_url_model'); //load the model for dealing with short URLs
        $shorty=$this->uri->segment(1);//get the segment the user requested;
        redirect($this->short_url_model->get_long_url($shorty));//direct the user to the long URL 
    }

    public function error_404() //a little error for when users enter an invalid short URL
    {
       
        $data['error']='Whoops cannot find that URL!';
        $this->load->view('get_url', $data);//load our single view
    }

}
