<?php

class Short_url_model extends CI_Model {

    //function will store long url in database
    function store_long_url()
    {

        $this->form_validation->set_rules('url', 'Url', 'trim|required|min_length[5]|max_length[250]|valid_url_format');

    	if($this->form_validation->run())
    	{
            $this->db->select("*");
			$this->db->from('urls');
			$this->db->where("long_url =",$this->input->post('url'));
			$query = $this->db->get();
			
			if($query->num_rows() < 1) {
				$this->db->insert('urls', array('long_url'=>$this->input->post('url')));
    		    return str_replace('=','-', base64_encode($this->db->insert_id()));
			}else{
				$record = $query->row();
				$this->db->set('count', 'count+1', FALSE);
				$this->db->where('long_url',$this->input->post('url'));
				$this->db->update('urls');
				return str_replace('=','-', base64_encode($record->id));
			}
    		
    	}
    }
	
	function record_count() {
        return $this->db->count_all("urls");
    }
	
	function get_report_data_url($limit, $start){
		$this->db->limit($limit, $start);
		
		$this->db->select("*");
		$this->db->from('urls');
		$this->db->order_by("count", "desc");		
		$query = $this->db->get();		
		return $query->result_array();
	}

	//get long url from short url
    function get_long_url($shorty='')
    {
    	$query=$this->db->get_where('urls', array('id'=> base64_decode(str_replace('-','=', $shorty))));
    	if($query->num_rows()>0)
    	{
    		foreach ($query->result() as $row)
			{
			    return $row->long_url;
			}
    	}
    	return '/error_404';
    }

 }
